<div align="center">

<a href="https://www.developer-corner.xyz/">![Nautilus-OS](https://user-images.githubusercontent.com/17615050/164157996-d9490dc5-566d-47df-ab6a-4e4fa8b054a4.png)</a>

Copyright © Socket Development. All Rights Reserved
</div>

# Nautilus OS
Open Source Windows 7 clone rebuilt with a custom Chromium/ChromeOS engine.

## Requirements
These requirements are simply a guideline of what we *suggest* to run **Nautilus OS** on. You *might* be able to run it on lower specs than these but we don't advise doing so.
 
### Minimal

 * **CPU**: Modern CPU with at least 6 cores or better
 * **RAM**: 8 GB DDR3 or better
 * **GPU**: Modern GPU with OpenGL/WebGL support

### Recommended

 * **CPU**: Modern CPU with at least 8 cores or better
 * **RAM**: 16 GB DDR4 or better
 * **GPU**: Modern GPU with OpenGL/WebGL support

## Features

* UEFI-ready
* Virtual shell hooks for seamless integration of Android and Linux subsystems
* Nautilus Commander

### Drives

 * **Nautilus**: Primary drive containing **Nautilus OS** (Default: `C:`)
 * **Android**: Minimal Android 10 drive for use with the Android subsystem (Default: `D:`)
 * **Linux**: Minimal CentOS drive for use with the Linux subsystem (Default: `E:`)
 
### Pre-Installed

* .NET Framework 4.8
* DirectX End-User Runtimes
* Media Player Classic
* Nautilus Explorer
* K-Lite Mega Codec Pack
* Microsoft Security Essentials

### Post-Installation Setup

 * Important and Security Updates
 * Windows Defender Updates
 * Latest Chromium Updates

